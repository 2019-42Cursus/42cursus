# ![42](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/42_Logo.svg/42px-42_Logo.svg.png)<span style="color:black">**Cursus**</span> - [42 School (Paris)](https://www.42.fr/)

## <span style="color:#00BABC">**Student : thzeribi [2019]**</span>

## Projects :
- ![#0fde00](https://via.placeholder.com/15/0fde00/000000?text=+) `Libft - 115/100`
- ![#0fde00](https://via.placeholder.com/15/0fde00/000000?text=+) `Netwhat - 100/100`
- ![#0fde00](https://via.placeholder.com/15/0fde00/000000?text=+) `Get_Next_Line - 107/100`
- ![#0fde00](https://via.placeholder.com/15/0fde00/000000?text=+) `FT_Printf - 100/100`
- ![#0fde00](https://via.placeholder.com/15/0fde00/000000?text=+) `FT_Server - 100/100`
- ![#0fde00](https://via.placeholder.com/15/0fde00/000000?text=+) `PushSwap - 84/100`
- ![#0fde00](https://via.placeholder.com/15/0fde00/000000?text=+) `Minitalk - 125/100`
- ![#FFD700](https://via.placeholder.com/15/0fde00/000000?text=+) `So_Long - 107/100`
- ![#FFD700](https://via.placeholder.com/15/B800F/000000?text=+) `Fractol - Cancel`
